#!/bin/bash

: << 'END'
Steps:

1 - Show a welcome message like "This is the installation of Marvin AIoT, a Common Lisp based Artificial Intelligence powered Internet of Things Open Source and Open Hardware project. ..."
2 - Check for dependencies (git, libsvm, mysqlclient-dev etc.) and install them if missing
3 - Check if any Lisp is installed (SBCL or CCL)
4 - If no Lisp is installed, determine the best of SBCL or CCL based on the system
    - Install Lisp
    - Install QuickLisp
5 - Check if Lisp has QuickLisp installed
6 - Determine local-projects folder from QuickLisp
7 - git clone https://gitlab.com/marvin-aiot/marvin-device/tools/marvin-installer.git into local-projects
8 - Start Lisp, ql:quickload :marvin-installer, execute (setup-marvin) and exit
9 - Setup marvin.service and enable it

END

# Variables to transmit information between the functions
lisp_impl=""
local_projects_path=""
lisp_init_file=""
quit_success=""
quit_fail=""

# Constants used throughout the script
INSTALLER_PATH=$( pwd )
TABLE_SPACING=70
RED='\033[0;31m'
GREEN='\033[0;32m'
NC='\033[0m'
UNINSTALL_SCRIPT='uninstall-marvin.sh'
TEMP_UNINSTALL_SCRIPT='uninstall-marvin.sh.temp'

#
# Add the first argument to the beginning of the uninstall script
#
function push_uninstall
{
    echo $1 | cat - $INSTALLER_PATH/$UNINSTALL_SCRIPT > $INSTALLER_PATH/$TEMP_UNINSTALL_SCRIPT && mv $INSTALLER_PATH/$TEMP_UNINSTALL_SCRIPT $INSTALLER_PATH/$UNINSTALL_SCRIPT
}

#
# Start a new uninstall script
#
function make_new_uninstall
{
    rm -f $INSTALLER_PATH/$UNINSTALL_SCRIPT
    echo "rm -f $UNINSTALL_SCRIPT" > $INSTALLER_PATH/$UNINSTALL_SCRIPT
}

#
# Finalize the uninstall script
#
function finalize_uninstall
{
    push_uninstall "#!/bin/bash"
    chmod u+x $INSTALLER_PATH/$UNINSTALL_SCRIPT
}

#
#
# $1 project name
# $2 project url
function clone
{
    project=$1
    url=$2
    installing="Cloning $project"
    delta=$(( $TABLE_SPACING - ${#installing} ))
    printf "%s%*s" "$installing" $delta ""

    if [ ! -d ${local_projects_path}$project ]
    then
	git clone $url ${local_projects_path}$project &>/dev/null
	if [ ! $? -eq 0 ]
	then
	    echo -e "[${REF}fail${NC}]"
	    return 1
	fi

	push_uninstall "sudo rm -rf ${local_projects_path}$project"
	echo -e "[${GREEN}ok${NC}]"
	return 0
    else
	echo -e "[${GREEN}skip${NC}]"
	return 0
    fi
}

#
# Show an introductory message to the user
#
function show_welcome_message
{
    echo "

Marvin AIoT, a Common Lisp based, Artificial Intelligence powered Internet of Things Open Source and Open Hardware project.

This is the installation of Marvin AIoT. 

Prerequisites:
- Marvin AIoT needs a MariaDB server to store its data. This server must be accessible to this installer.
- This script expects systemd is used for services.

During the installation 'sudo' will be used to execute commands that need higher priviledges. This are, for example, creating folders in /opt, creating links in /usr/bin or registering and starting Marvin AIoT as a service.
" | fmt -w $TABLE_SPACING
}

#
# Verify that programs used by this installation script are available
#
function check_necessary_programs
{
    echo ""
    echo ""
    echo 'Checking necessary programs to continue...'

    packages=( git login curl make wget bzip2 grep sed )
    packages_missing=0

    checking="Checking "
    checking_length=${#checking}

    for i in "${packages[@]}"
    do
	:
	delta=$(( $TABLE_SPACING - ${#i} - $checking_length ))
	printf "%s%s%*s" "$checking" "$i" $delta ""
	command=$( command -v "$i" )
	if [ -z command ]
	then
	    echo -e "[${RED}missing${NC}]"
	    packages_missing=1
	else
	    echo -e "[${GREEN}ok${NC}]"
	fi
    done

    return $packages_missing
}

#
# Verify the Marvin AIoT specific depencendies are available and optionally install them via apt install if not
#
function check_necessary_packages
{
    echo ""
    echo ""
    echo 'Checking necessary packages to continue...'

    packages=( build-essential libssl1.1 libsvm3 libmariadb-dev-compat )
    missing_packages=""

    checking="Checking "
    checking_length=${#checking}

    for i in "${packages[@]}"
    do
	:
	found=$( dpkg-query -W -f='${Status}' "$i" 2>/dev/null | grep "ok installed" )
	delta=$(( $TABLE_SPACING - ${#i} - $checking_length ))
	printf "%s%s%*s" "$checking" "$i" $delta ""
	if [ -z "$found" ]
	then
	    echo -e "[${RED}missing${NC}]"
	    missing_packages+="$i "
	else
	    echo -e "[${GREEN}ok${NC}]"
	fi
    done

    if [ -n "$missing_packages" ]
    then
	echo ''
	echo 'Some packages are missing. These are:'
	echo "$missing_packages" | fmt -w $TABLE_SPACING
	echo ''
	echo -e 'Would you like to install the missing packages now? (y/n) \c'
	
	read
	if [ "$REPLY" != "y" ]
	then
	    return 1
	else
	    installing="Installing packages"
	    delta=$(( $TABLE_SPACING - ${#installing} ))
	    printf "%s%*s" "$installing" $delta ""
	    sudo apt update >/dev/null 
	    sudo apt install $missing_packages # >/dev/null
	    echo -e "[${GREEN}ok${NC}]"
	    push_uninstall "sudo apt remove -y $missing_packages"
	fi    
    fi

    return 0
}

#
# Identify if a Common Lisp implementation is already installed or install one (according to the target architecture)
#
function find_or_install_lisp
{
    echo ""
    echo ""
    echo 'Looking for a Common Lisp implementation...'
    
    implementations=("sbcl" "ccl")
    looking_for="Looking for "
    looking_for_length=${#looking_for}

    declare -A lisp_implementations
    
    for i in "${implementations[@]}"
    do
	:
	delta=$(( $TABLE_SPACING - ${#i} -  $looking_for_length ))
	printf "%s%s%*s" "$looking_for" "$i" $delta ""
	lisp_command=$( command -v "$i" )
	if [ -n "$lisp_command" ]
	then
	    echo -e "[${GREEN}ok${NC}]"
	    lisp_implementations[$i]=1
	else
	    echo -e "[${RED}missing${NC}]"
	    lisp_implementations[$i]=0
	fi
    done
    
    architecture=""

    regex_amd64="^x86_64$"
    regex_arm="^arm.*$"
    machine=$(uname -m)
    
    if [[ "${machine}" =~ $regex_amd64 ]]; then
	architecture="amd64"
    elif [[ "${machine}" =~ $regex_arm ]]; then
	dpkg --print-architecture | grep -q "arm64" && architecture="arm64" || architecture="arm"
    fi

    # SBCL - AMD64 & ARM64
    if [ "$architecture" == "amd64" ] || [ "$architecture" == "arm64" ]
    then
	quit_success="(quit :unix-status 0)"
	quit_fail="(quit :unix-status 1)"
	lisp_options="--noinform --end-runtime-options --disable-debugger"

	if [ ${lisp_implementations["sbcl"]} -eq 1 ]
	then
	    lisp_impl=$( command -v "sbcl" )
	    lisp_init_file=~/.sbclrc
	    echo "Using existing SBCL with version $( $lisp_impl --version )"
	    return 0
	fi

	if [ -d /opt/sbcl ]
	then
	    echo -e "${RED}The path /opt/sbcl/ already exists. Aborting installation.${NC}"
	    return 1
	fi

	detail=""
	if [ "$architecture" == "amd64" ]
	then
	    detail="1.5.4-x86-64"
	else
	    detail="1.4.2-arm64"
	fi

	installing="Installing SBCL"
	delta=$(( $TABLE_SPACING - ${#installing} ))
	printf "%s%*s" "$installing" $delta ""
	
	url="http://prdownloads.sourceforge.net/sbcl/sbcl-$detail-linux-binary.tar.bz2"
	wget $url -O /tmp/sbcl.tar.bz2 &>/dev/null 
	if [ ! $? -eq 0 ]
	then
	    echo -e "[${RED}fail${NC}]"
	    return 1
	fi

	mkdir /tmp/sbcl
	tar -xvjf /tmp/sbcl.tar.bz2 --strip-components=1 -C /tmp/sbcl/ &>/dev/null
	if [ ! $? -eq 0 ]
	then
	    echo -e "[${RED}fail${NC}]"
	    return 1
	fi

	rm -f /tmp/sbcl.tar.bz2

	sudo mkdir -p /opt/sbcl
	push_uninstall "sudo rm -rf /opt/sbcl"
	sudo chown ${USER:=$(/usr/bin/id -run)}:$USER /opt/sbcl
	
	cd /tmp/sbcl
	INSTALL_ROOT=/opt/sbcl sh install.sh >/dev/null
	if [ ! $? -eq 0 ]
	then
	    echo -e "[${RED}fail${NC}]"
	    return 1
	fi

	push_uninstall "unset SBCL_HOME INSTALL_ROOT SBCL SBCL_BUILDING_CONTRIB SBCL_PWD INSTALL_DIR"
	#sudo sh install.sh
	cd $INSTALLER_PATH
	rm -rf /tmp/sbcl

	sudo ln -s /opt/sbcl/bin/sbcl /usr/bin/sbcl
	push_uninstall "sudo rm -f /usr/bin/sbcl"

	lisp_impl=$( command -v "sbcl" )
	lisp_init_file=~/.sbclrc
	touch $lisp_init_file
	push_uninstall "rm -f ~/.sbclrc"
	echo -e "[${GREEN}ok${NC}]"
    fi

    # CCL - ARM32
    if [ "$architecture" == "arm" ]
    then
	quit_success="(quit 0)"
	quit_fail="(quit 1)"

	if [ ${lisp_implementations["ccl"]} -eq 1 ]
	then
	    lisp_impl=$( command -v "ccl" )
	    lisp_options=""
	    lisp_init_file=~/.ccl-init.lisp
	    echo "Using existing CCL with version $( $lisp_impl --version )"
	    return 0
	fi

	if [ -d /opt/ccl ]
	then
	    echo -e "${RED}The path /opt/ccl/ already exists. Aborting installation.${NC}"
	    return 1
	fi
	
	installing="Installing CCL"
	delta=$(( $TABLE_SPACING - ${#installing} ))
	printf "%s%*s" "$installing" $delta ""
	
	wget https://github.com/Clozure/ccl/releases/download/v1.11.5/ccl-1.11.5-linuxarm.tar.gz -O /tmp/ccl.tar.gz &>/dev/null
	if [ ! $? -eq 0 ]
	then
	    echo -e "[${RED}fail${NC}]"
	    return 1
	fi

	mkdir /tmp/ccl
	tar -xvzf /tmp/ccl.tar.gz --strip-components=1 -C /tmp/ccl/ &>/dev/null
	if [ ! $? -eq 0 ]
	then
	    echo -e "[${RED}fail${NC}]"
	    return 1
	fi

	rm -f /tmp/ccl.tar.gz
	cd /tmp
	
	sudo mkdir -p /opt/ccl
	push_uninstall "sudo rm -rf /opt/ccl"
	sudo chown ${USER:=$(/usr/bin/id -run)}:$USER /opt/ccl
	cp -r /tmp/ccl/* /opt/ccl/
	if [ ! $? -eq 0 ]
	then
	    echo -e "[${RED}fail${NC}]"
	    return 1
	fi

	cd $INSTALLER_PATH
	rm -rf /tmp/ccl

	sudo ln -s /opt/ccl/armcl /usr/bin/ccl
	push_uninstall "sudo rm -f /usr/bin/ccl"
	
	lisp_impl=$( command -v "ccl" )
	lisp_options=""
	lisp_init_file=~/.ccl-init.lisp
	touch $lisp_init_file
	push_uninstall "rm -f ~/.ccl-init.lisp"
	echo -e "[${GREEN}ok${NC}]"
    fi

    if [ -z $lisp_impl ]
    then
	echo -e "${RED}The system architecture '${machine}' is not yet supported. Please contact the maintaners of Marvin AIoT.${NC}"
	return 1
    fi

    return 0
}

#
# Check if QuickLisp was already installed or install it if not
#
function check_or_install_quicklisp
{
    echo ""
    echo ""
    installing="Looking for QuickLisp"
    delta=$(( $TABLE_SPACING - ${#installing} ))
    printf "%s%*s" "$installing" $delta ""
    
    ret=$( $lisp_impl $lisp_options --eval "(if (find-package :ql) $quit_success $quit_fail)" ) >/dev/null
    if [ $? -eq 0 ]
    then
	echo -e "[${GREEN}ok${NC}]"
	return 0
    fi

    if [ -d /opt/quicklisp ]
    then
	echo -e "[${RED}fail${NC}]"
	echo -e "${RED}The path /opt/quicklisp/ already exists. Aborting installation.${NC}"
	return 1
    fi

    # Install QuickLisp
    wget http://beta.quicklisp.org/quicklisp.lisp -O /tmp/quicklisp.lisp &>/dev/null
    sudo mkdir -p /opt/quicklisp
    push_uninstall "sudo rm -rf /opt/quicklisp"
    
    sudo chown ${USER:=$(/usr/bin/id -run)}:$USER /opt/quicklisp
    
    $lisp_impl $lisp_options --load /tmp/quicklisp.lisp --eval "(quicklisp-quickstart:install :path \"/opt/quicklisp\")" --eval "(uiop:quit 0)" &>/dev/null
    if [ ! $? -eq 0 ]
    then
	echo -e "[${RED}fail${NC}]"
	return 1
    fi

    
    # ql:add-to-init-file blocks waiting for user input. So we manually insert the initialization code for QuickLisp.
    #       CCL: "~/.ccl-init.lisp"
    #       SBCL: "/.sbclrc"
    #
    echo '
;; vvvvvvvvvvvvvvv
;; ======= Added by Marvin AIoT installer =======

#-quicklisp
(let ((quicklisp-init #P"/opt/quicklisp/setup.lisp"))
  (when (probe-file quicklisp-init)
    (load quicklisp-init)))


;; ^^^^^^^^^^^^^^^
' >> ${lisp_init_file}

    # @TODO push_uninstall remove text "VVVV ... ^^^^" with sed?
    
    rm -f /tmp/quicklisp.lisp

    echo -e "[${GREEN}ok${NC}]"
    return 0
}

#
# Get the projects needed to bootstrap the installer
#
function clone_bootstrap_projects
{
    echo ""
    echo ""
    echo 'Cloning bootstrap projects from git...'
    
    regex="local-projects: '(.*)'"
    
    # Use ql:*local-project-directories*
    ret=$( $lisp_impl $lisp_options --eval "(progn (format t \"local-projects: '~a'~%\" (car ql:*local-project-directories*)) (uiop:quit 0))" )
    if [[ $ret =~ $regex ]]
    then
        local_projects_path="${BASH_REMATCH[1]}"
    else
	echo -e "${RED}Could not identify path to QuickLisp local projects.${NC}"
	return 1
    fi

    clone trivial-utilities https://gitlab.com/ediethelm/trivial-utilities.git
    if [ ! $? -eq 0 ]
    then
	return 1
    fi

    clone trivial-pooled-database https://gitlab.com/ediethelm/trivial-pooled-database.git
    if [ ! $? -eq 0 ]
    then
	return 1
    fi

    clone trivial-object-lock https://gitlab.com/ediethelm/trivial-object-lock.git
    if [ ! $? -eq 0 ]
    then
	return 1
    fi

    clone trivial-hashtable-serialize https://gitlab.com/ediethelm/trivial-hashtable-serialize.git
    if [ ! $? -eq 0 ]
    then
	return 1
    fi

    clone trivial-repl https://gitlab.com/ediethelm/trivial-repl.git
    if [ ! $? -eq 0 ]
    then
	return 1
    fi

    clone trivial-gitlab-api https://gitlab.com/ediethelm/trivial-gitlab-api.git
    if [ ! $? -eq 0 ]
    then
	return 1
    fi

    clone trivial-json-codec https://gitlab.com/ediethelm/trivial-json-codec.git
    if [ ! $? -eq 0 ]
    then
	return 1
    fi

    clone piggyback-parameters https://gitlab.com/ediethelm/trivial-piggyback-parameters.git
    if [ ! $? -eq 0 ]
    then
	return 1
    fi

    clone marvin-installer https://gitlab.com/marvin-aiot/marvin-device/tools/marvin-installer.git
    if [ ! $? -eq 0 ]
    then
	return 1
    fi

    return 0
}

#
# Execute the setup procedure of Marvin AIoT
#
function setup_marvin
{
    echo ""
    echo ""
    echo 'Setting up Marvin AIoT...'
    echo ''
    echo 'It might take a few minutes (depending on the hardware and internet connection) to load.'

    $lisp_impl $lisp_options --eval "(ql:quickload :marvin-installer :silent t)" --eval "(uiop:quit (marvin-installer:setup-marvin :automated t))" --eval "(uiop:quit 1)"
    if [ $? != 0 ]
    then
	echo -e "${RED}Setup procedure of Marvin AIoT was aborted.${NC}"
	return 1
    fi

    installing="Setting service up"
    delta=$(( $TABLE_SPACING - ${#installing} ))
    printf "%s%*s" "$installing" $delta ""

    if [ -d /etc/systemd/system/marvin.service ]
    then
	echo -e "${RED}The service description '/etc/systemd/system/marvin.service' already exists.${NC}"
	return 1
    fi

    echo "[Unit]
Description=Marvin AI Of Things
ConditionPathExists=${local_projects_path}marvin-base/deployment/start.lisp
After=network.target mariadb.service
Wants = mariadb.service

[Service]
User = marvin
Group = marvin
Type = simple
WorkingDirectory=${local_projects_path}marvin-base
ExecStart=${lisp_impl} $lisp_options --load ${local_projects_path}marvin-base/deployment/start.lisp
Restart=always
# Restart service after 10 seconds if service crashes
RestartSec=10

# Output to syslog
StandardOutput=syslog+console
StandardError=syslog+console
SyslogIdentifier=marvin

[Install]
WantedBy=multi-user.target
" >> marvin.service
    
    sudo mv marvin.service /etc/systemd/system/marvin.service
    if [ $? != 0 ]
    then
	echo -e "${RED}Could not copy service description to systemd.${NC}"
	return 1
    fi
    push_uninstall "sudo rm -rf /etc/systemd/system/marvin.service"
    
    sudo systemctl daemon-reload >/dev/null
    if [ $? != 0 ]
    then
	echo -e "${RED}Reloading of systemd deamon failed.${NC}"
	return 1
    fi
    push_uninstall "sudo systemctl daemon-reload"
    
    sudo systemctl start marvin >/dev/null
    if [ $? != 0 ]
    then
	echo -e "${RED}Starting Marvin AIoT service failed.${NC}"
	return 1
    fi
    push_uninstall "sudo systemctl stop marvin"
    
    sudo systemctl enable marvin >/dev/null
    if [ $? != 0 ]
    then
	echo -e "${RED}Could not enable Marvin AIoT service${NC}"
	return 1
    fi
    push_uninstall "sudo systemctl disable marvin"

    echo -e "[${GREEN}ok${NC}]"
    return  0
}

#
# Entry point of the installer itself
#
function main
{
    show_welcome_message
    echo -e "Ready to start the installation? (y/n) \c"
    read
    if [ "$REPLY" != "y" ]
    then
	echo -e "Exiting installation"
	return 1
    fi

    make_new_uninstall
    
    check_necessary_programs &&
	check_necessary_packages &&
	find_or_install_lisp &&
	check_or_install_quicklisp &&
	clone_bootstrap_projects &&
	setup_marvin
    if [ $? != 0 ]
    then
	finalize_uninstall
	echo -e "${RED}Aborting installation${NC}"
	echo ''
	echo -e "${RED}Reverting changes made${NC}"
	cd $INSTALLER_PATH
	. $INSTALLER_PATH/$UNINSTALL_SCRIPT
	echo -e "${RED}The installation has failed.${NC}"
	return 1
    fi

    finalize_uninstall
    mv $INSTALLER_PATH/$UNINSTALL_SCRIPT ~/$UNINSTALL_SCRIPT
    rm -f $INSTALLER_PATH/$TEMP_UNINSTALL_SCRIPT
    echo "A script to uninstall Marvin AIoT was added to ~/$UNINSTALL_SCRIPT"
    
    echo -e "${GREEN}Installation complete${NC}"
    
    return 0
}

main

