
("CREATE FUNCTION `GET_NEXT_ID` ()
RETURNS INT(10) UNSIGNED 
NOT DETERMINISTIC READS SQL DATA # actually MODIFIES SQL DATA, but there is a bug in MySQL #93807
BEGIN
  DECLARE next_id INT;
  SET next_id = 0;

  SELECT `value` from `_OPTIONS_` where `parameter` = 'NEXT-ID' into next_id;

  UPDATE `_OPTIONS_` SET `value` = CAST((1 + next_id) AS CHAR) where `parameter` = 'NEXT-ID';

  RETURN next_id;
END"

"CREATE FUNCTION `GET_LAST_CREATED_ID` ()
RETURNS INT(10) UNSIGNED 
NOT DETERMINISTIC READS SQL DATA
BEGIN
  DECLARE next_id INT;
  SET next_id = 0;

  SELECT `value` from `_OPTIONS_` where `parameter` = 'NEXT-ID' into next_id;

  RETURN next_id - 1;
END"

"CREATE TABLE `LangzeitSpeicher`
  (`DING-ID` bigint(20) UNSIGNED NOT NULL COMMENT 'The ID of the Ding',
   `ÄNDERUNGSDATUM` int(10) UNSIGNED NOT NULL COMMENT 'Timestamp of this modification',
   `P-LIST` mediumtext NOT NULL COMMENT 'The content of the Ding''s P-List',
   `KLASSE` bigint(20) NOT NULL COMMENT 'The class of this Ding',
   PRIMARY KEY (`DING-ID`),
   KEY (`KLASSE`))
  ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Marvin''s LangzeitSpeicher';"


"CREATE TABLE `DingAssociations`
  (`DING-ID` bigint(20) UNSIGNED NOT NULL COMMENT 'The ID of the Ding',
   `UPDATE-CYCLES` int(10) UNSIGNED NOT NULL COMMENT 'A counter of updates on this Ding',
   `ASSOCIATIONS` mediumtext NOT NULL COMMENT 'The list of Associations',
   PRIMARY KEY (`DING-ID`))
  ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Marvin''s Associations';"


"CREATE TABLE `_OPTIONS_`
  (`parameter` varchar(64) NOT NULL,
   `value` varchar(256) NOT NULL,
   PRIMARY KEY (`parameter`))
  ENGINE=InnoDB DEFAULT CHARSET=utf8;"

"INSERT INTO `_OPTIONS_` (`parameter`, `value`) VALUES
('NEXT-ID', '19'),
(':INSTALLED-PLUGINS', '<>'),
(':ENABLED-PLUGINS', '<>');"

"INSERT INTO `LangzeitSpeicher` (`DING-ID`, `ÄNDERUNGSDATUM`, `P-LIST`, `KLASSE`) VALUES
(0,3712141817,'<#DING-8?,#DING-12?,\":PERSISTENT\",T>',-1),
(1,3712141817,'<\":DEVICE-ID\",1,#DING-8?,#DING-9?,\":PERSISTENT\",T>',0),
(2,3712141817,'<#DING-8?,#DING-13?>',0),
(3,3712141817,'<#DING-8?,#DING-14?>',0),
(4,3712141817,'<#DING-8?,#DING-15?>',0),
(5,3712141817,'<#DING-8?,#DING-16?>',0),
(6,3712141817,'<#DING-8?,#DING-17?>',0),
(7,3712141817,'<#DING-8?,#DING-18?>',0),
(8,3712157782,'<\":GRAPHIE\",\"Name\">',7),
(9,3712157782,'<\":GRAPHIE\",\"Marvin\">',7),
(10,3712157782,'<\":GRAPHIE\",\"Beschreibung\">',7),
(11,3712157782,'<\":GRAPHIE\",\"Einheit\">',7),
(12,3712157782,'<\":GRAPHIE\",\"Ding\">',7),
(13,3712157782,'<\":GRAPHIE\",\"Frame\">',7),
(14,3712157782,'<\":GRAPHIE\",\"K-Line\">',7),
(15,3712157782,'<\":GRAPHIE\",\"PluginBase\">',7),
(16,3712157782,'<\":GRAPHIE\",\"Plugin-Response\">',7),
(17,3712157782,'<\":GRAPHIE\",\"Action\">',7),
(18,3712157782,'<\":GRAPHIE\",\"Wort\">',7);"

"INSERT INTO `DingAssociations` (`DING-ID`, `UPDATE-CYCLES`, `ASSOCIATIONS`) VALUES
(0,0,'<>'),
(1,0,'<<#DING-2?,1.0f0>,<#DING-3?,1.0f0>,<#DING-4?,1.0f0>,<#DING-5?,1.0f0>,<#DING-6?,1.0f0>,<#DING-7?,1.0f0>,<#DING-8?,1.0f0>,<#DING-9?,1.0f0>>'),
(2,0,'<>'),
(3,0,'<>'),
(4,0,'<>'),
(5,0,'<>'),
(6,0,'<>'),
(7,0,'<>'),
(8,0,'<>'),
(9,0,'<<#DING-1? 1.0f0>>'),
(10,0,'<>'),
(11,0,'<>'),
(12,0,'<>'),
(13,0,'<>'),
(14,0,'<>'),
(15,0,'<>'),
(16,0,'<>'),
(17,0,'<>'),
(18,0,'<>');")
