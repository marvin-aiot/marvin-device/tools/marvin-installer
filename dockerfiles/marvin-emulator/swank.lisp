
(ql:quickload :bordeaux-threads :silent t)
(ql:quickload :log4cl :silent t)
(ql:quickload :swank :silent t)

;;(defparameter *shutdown-lock* (bordeaux-threads:make-lock))
;;(defparameter *shutdown-request-cv* (bordeaux-threads:make-condition-variable))

(log:config :pattern "%&%<%I%;<;;>;-5p [%D{%H:%M:%S}]%:; [;;];t %g{}{}{:downcase} (%C{}{ }{:downcase})%2.2N - %_%m%>%n")
(log:config :daily "log/marvin.%Y%m%d.log" :pattern "<%p> [%D{%H:%M:%S}] [%t] (%C{}{ }{:downcase}) - %m%n")

(setf swank::*LOOPBACK-INTERFACE* "0.0.0.0")
(swank-loader:init)
(swank:create-server :port 4005 :dont-close t)

(ql:quickload :device-emulator)
(device-emulator:start-emulation)

(loop do (sleep 60))
;;(bordeaux-threads:with-lock-held (*shutdown-lock*)
;;  (bordeaux-threads:condition-wait *shutdown-request-cv* *shutdown-lock*))
