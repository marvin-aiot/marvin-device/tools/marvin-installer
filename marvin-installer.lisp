;;;; Copyright (C) 2019 Eric Diethelm

;;;; This program is free software: you can redistribute it and/or modify
;;;; it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation, either version 3 of the License, or
;;;; (at your option) any later version.

;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.

;;;; You should have received a copy of the GNU General Public License
;;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(in-package :marvin-installer)

(defvar *marvin-plugins-group* 4694788)

(defparameter *setup-steps* '())
(defparameter *autodeploy-config* nil)
(defparameter *exit-code* 1)

(defmacro define-setup-step (name documentation &body body)
  (declare (ignore documentation))
  `(push (cons ,name (trivial-utilities:alambda () ,@body)) *setup-steps*))

(defun load-autodeploy-config (file #|"./auto-deployment.cnf"|#)
  (unless (uiop:file-exists-p file)
    (log:warn "Configuration file for automatic deployment not found ('~a')." file)
    (return-from load-autodeploy-config nil))

  (with-open-file (*standard-input* file)
    (let ((autodeploy-config (read)))
      ;; Verify config is valid property list
      (unless (and (eq 0 (mod (length autodeploy-config) 2))
		   (progn
		     (iterate:iterate
		      (iterate:for elm on autodeploy-config by #'cddr)
		      (unless (keywordp (car elm))
			(return nil)))
		     t))
	(error "The auto-deploy configuration file '~a' contains errors." file))

      (setf *autodeploy-config* autodeploy-config))))

(defun merge-autodeploy-config (config)
  (iterate:iterate
   (iterate:for elm on config by #'cddr)
   (setf (getf *autodeploy-config* (car elm)) (cadr elm))))

;; AutoDeploy value: True -> Do it
;; AutoDeploy value: False -> Don't do it
;; AutoDeploy value: missing -> check next conditions
(defun check-boolean (param question &rest args)
  (trivial-utilities:aif (and param
			      (member param *autodeploy-config*))
			 (cadr it)
			 (progn
			   (format t "~a ([Y]es/[N]o) "
				   (apply #'format nil `(,question ,@args)))
			   (finish-output)
			   (let ((ans (read-line)))
			     (format t "~%")
			     (and (stringp ans) (or (eq (char ans 0) #\y) (eq (char ans 0) #\Y)))))))

(defun check (param question &rest args)
  (trivial-utilities:aif (and param
			      (member param *autodeploy-config*))
			 (cadr it)
			 (progn
			   (apply #'format t `(,question ,@args))
			   (finish-output)
			   (let ((ans (read-line)))
			     (format t "~%")
			      ans))))

;; How to differenciate between nil value and no value?
(defun get-autodeploy-parameter (param)
  (getf *autodeploy-config* param))

(define-setup-step :start
    "Check if marvin is already running
   Yes - Inform the user and goto :exit
   No  - Goto :load-config"
  (format t "~%This preocedure will setup Marvin AIoT.~%")
  (let ((running? (uiop:find-symbol* "RUNNING?" :marvin nil)))
    (if (and running? (funcall running?))
	(progn
	  (format t "Marvin is already running. Please stop it and re-execute this.~%")
	  (return-from self :abort)))
    (return-from self :update-code)))

(define-setup-step :update-code
    "Ask is the codebase should be updated now
     Yes - 
     No - "
  ;; @TODO This list of libraries should be stored in a separate file and loaded here.
  (let ((libraries (list (list "ediethelm" "trivial-utilities" "")
			 (list "ediethelm" "trivial-object-lock" "")
			 (list "ediethelm" "trivial-json-codec" "")
			 (list "ediethelm" "trivial-hashtable-serialize" "")
			 (list "ediethelm" "trivial-pooled-database" "")
			 (list "ediethelm" "trivial-timer" "")
			 (list "ediethelm" "trivial-filter" "")
			 (list "ediethelm" "trivial-continuation" "")
			 (list "ediethelm" "trivial-variable-bindings" "")
			 (list "ediethelm" "trivial-monitored-thread" "")
			 (list "ediethelm" "trivial-repl" "")
			 (list "ediethelm" "trivial-difference" "")
			 (list "ediethelm" "trivial-action" "")
			 (list "ediethelm" "piggyback-parameters" "")
			 (list "ediethelm" "communication-protocol" "")
			 (list "marvin-aiot/marvin-device" "marvin-base" ""))))
    
    ;; Check if the needed libraries are already installed
    (let ((missing-projects (iterate:iterate
			      (iterate:for (username project group) in libraries)
			      (unless (asdf:find-system project nil)
				(iterate:collect (list username project group))))))

      ;; @TODO When comming from install-marvin.sh no questions should be asked!
      (when missing-projects
	(if (check-boolean :install-missing-dependencies
			   "Some necessary libraries for Marvin are missing. Should I install them now?")
	    (progn
	      (iterate:iterate
		(iterate:for (username project group) in missing-projects)
		(trivial-gitlab-api:clone-project username
						  project
						  group
						  (uiop/common-lisp:merge-pathnames
						   "local-projects/"
						   ql-setup:*quicklisp-home*))))
	    (progn
	      (format t "See the necessary libraries in the documentation and manually install them.")
	      (return-from self :abort))))

      (trivial-utilities:awhen (nset-difference libraries missing-projects :test #'string= :key #'second)
	(when (check-boolean :clean-installed-libraries
			     "Should I now update all the previously existing Marvin libraries?")
	  (if (check-boolean nil "~%NOTE: Any local changes will be destroyed! Are you sure?")
	      (iterate:iterate
		(iterate:for (username project group) in it)
		(trivial-gitlab-api:revert-project project group (asdf:system-source-directory project) :is-project-path t))
	      (unless (check-boolean nil "Please continue with caution, since some of the libraries might be in an incompatible version.~%Do you want to continue the installation procedure?")
		(return-from self :abort)))))))
  
  (return-from self :load-config))

(define-setup-step :load-config
    "Try to initialize piggyback-parameters
  If error - ask the user if a new config should be created
    Yes - Goto :setup-db
    No - Ask for the path of of the config file (if empty, stop), copy it to the expected location and goto :start
  If no error - Goto :setup-db"
  (let ((config-file (uiop/common-lisp:merge-pathnames
		      #P"config"
		      (asdf:system-source-directory :marvin-base))))
    
    (handler-bind
	((piggyback-parameters:file-not-found-error
	  #'(lambda (c)
	      (declare (ignore c))
	      (if (check-boolean :create-config-file
				 "No configuration file was found. Do you want me to create one now?")
		  (progn
		    ;; Create the config file
		    (with-open-file (f config-file :direction :output
				       :if-exists :supersede
				       :if-does-not-exist :create))
		    (return-from self :load-config))
		  (return-from self :abort)))))

      (piggyback-parameters:set-configuration-file config-file)))
  (return-from self :setup-db-connection))

(define-setup-step :setup-db-connection
    "Ask for the hostname/ip of the MySQL server
     Check server is reachable
       Yes - Goto :setup-db-root-access
       No - Inform the user and goto :exit"
  (let* ((host (check :db-host
		      "Please enter the hostname/IP of the DB server: "))
	 (sock (ignore-errors
		 (usocket:socket-connect host 3306 :timeout 0.5))))

    (if sock
	(progn
	  (usocket:socket-close sock)

	  ;; Save host and port to config file
	  (setf (piggyback-parameters:get-value :db-host :file-only) host)
	  (return-from self :setup-db-root-access))
	(progn
	  (format t "Could not contact MySQL server at ~a.~%" host)
	  ;; Ask the user if a new host should be tried.
	  (when (check-boolean nil "Would you like to repeat the DB host/IP?")
	       (return-from self :setup-db-connection))
	  (return-from self :abort)))))

;; @TODO We need a method of skipping the use of root access to the DB.
(define-setup-step :check-db-root-access-needed
    "Ask whether root access to the DB is needed.
       Yes - continue to :setup-db-root-access
       No - Ask for Marvin user credentials and continue to :create-schema"
  (when (check-boolean :db-use-existing-user "Does a Marvin user already exist in the DB?")
      (let ((username (check :db-user
			     "Please enter the username of the DB user: "))
	    (passwd (check :db-passwd
			   "Please enter the password of the DB user: ")))

	   (setf (piggyback-parameters:get-value :db-username :file-only) username)
	   (setf (piggyback-parameters:get-value :db-passwd :file-only) passwd))
      (return-from self :create-schema))
  (return-from self :setup-db-root-access))

(define-setup-step :setup-db-root-access
    "Ask for the hostname/ip of the MySQL server
     Check server is reachable
       Yes - Goto :create-user
       No - Inform the user and goto :exit
     Ask for root credentials and goto :create-user"
  
  (let ((username (check :db-root-user
			 "Please enter the username of the DB admin user: "))
	(passwd (check :db-root-passwd
		       "Please enter the password of the DB admin user: "))
	(db-connection nil))
    (unwind-protect
	 (progn
	   (setf db-connection (ignore-errors (dbi:connect :mysql
							   :username username
							   :password passwd
							   :host (piggyback-parameters:get-value :db-host :file-only)
							   :database-name nil)))
	   (unless db-connection
	     (format t "Unable to login to the DB server. Please check your credentials.~%")
	     ;; Ask the user if other credentials should be tried.
	     (when (check-boolean nil "Would you like to repeat the DB credentials?")
	       (return-from self :setup-db-root-access))
	     (return-from self :abort))

	   (setf (piggyback-parameters:get-value :db-admin-username :memory-only) username)
	   (setf (piggyback-parameters:get-value :db-admin-passwd :memory-only) passwd))

      (when db-connection
	(dbi:disconnect db-connection))))
  
  (return-from self :create-user))

(define-setup-step :create-user
    "Check if a user named 'marvin' already exists
     Yes - Inform the user and goto :exit
     No - Create a new DB user named 'marvin' and generate a password and goto :create-schema"
  (let ((db-connection nil))
    (unwind-protect
	 (progn
	   (setf db-connection (ignore-errors
				 (dbi:connect :mysql
					      :username (piggyback-parameters:get-value :db-admin-username)
					      :password (piggyback-parameters:get-value :db-admin-passwd)
					      :host (piggyback-parameters:get-value :db-host)
					      :database-name nil)))

	   (unless db-connection
	     (format t "Unable to login to the DB server. Please check your credentials.~%")
	     (return-from self :abort))

	   (let ((result (dbi:execute (dbi:prepare db-connection "SET names 'utf8';"))))
	     ;; Consume the result
	     (loop for row = (dbi:fetch result)
		while row))

	   (trivial-utilities:awhen (dbi:fetch-all
				     (dbi:execute
				      (dbi:prepare db-connection "select USER, HOST from mysql.user where user = 'marvin'")))
	     (format t "Apparently a user named 'marvin' already exists in the database.~%")
	     (if (check-boolean :db-drop-existing-user
				"May this user be dropped so a new one can be created?")
		 (loop for item in it
		    do (progn
			 (format t "Dropping user '~a'@'~a'.~%" (second (member :user item)) (second (member :host item)))
			 (dbi:execute
			  (dbi:prepare db-connection
				       (format nil "drop user '~a'@'~a'" (second (member :user item)) (second (member :host item)))))))
		 (return-from self :abort)))

	   ;; Create user 'marvin'
	   (setf (piggyback-parameters:get-value :db-username :file-only) "marvin")
	   ;; Create a password
	   (setf (piggyback-parameters:get-value :db-passwd :file-only) (format nil "~x" (sxhash (random 999999))))
	   
	   (let ((create-user-stmt (concatenate 'string
						"CREATE USER 'marvin'@'"
						(if (or (string-equal (piggyback-parameters:get-value :db-host)
								      "localhost")
							(string-equal (piggyback-parameters:get-value :db-host)
								      "127.0.0.1"))
						    (piggyback-parameters:get-value :db-host)
						    "%")
						"' IDENTIFIED WITH mysql_native_password"))
		 (set-password-stmt (concatenate 'string
				                 "SET PASSWORD FOR 'marvin'@'"
						(if (or (string-equal (piggyback-parameters:get-value :db-host)
								      "localhost")
							(string-equal (piggyback-parameters:get-value :db-host)
								      "127.0.0.1"))
						    (piggyback-parameters:get-value :db-host)
						    "%")
						"' = PASSWORD('"
						 (piggyback-parameters:get-value :db-passwd)
						"')")))
	     (let ((res (dbi:fetch-all
			 (dbi:execute
			  (dbi:prepare db-connection create-user-stmt)))))
	       ;; @TODO Check that it has worked
	       (declare (ignore res)))

	     (let ((res (dbi:fetch-all
			 (dbi:execute
			  (dbi:prepare db-connection set-password-stmt)))))
	       ;; @TODO Check that it has worked
	       (declare (ignore res)))))

      (when db-connection
	(dbi:disconnect db-connection))))
  (return-from self :create-schema))

(define-setup-step :create-schema
    "Check if a schema named 'marvin' exists
     Yes - Inform the user and goto :abort
     No - Create shema according to marvin_base.sql"
  
  (let ((db-connection nil))
    (unwind-protect
	 (progn
	   (setf db-connection (ignore-errors (dbi:connect :mysql
							   :username (piggyback-parameters:get-value :db-admin-username)
							   :password (piggyback-parameters:get-value :db-admin-passwd)
							   :host (piggyback-parameters:get-value :db-host)
							   :database-name nil)))

	   (unless db-connection
	     (format t "Unable to login to the DB server as admin. Please check your credentials.~%")
	     (return-from self :abort))

	   
	   (let ((result (dbi:execute (dbi:prepare db-connection "SET names 'utf8';"))))
	     ;; Consume the result
	     (loop for row = (dbi:fetch result)
		while row))
	   
	   ;; Check the shema 'marvin' does not yet exist
	   (when (dbi:fetch-all
		  (dbi:execute
		   (dbi:prepare db-connection "select SCHEMA_NAME from INFORMATION_SCHEMA.SCHEMATA where SCHEMA_NAME = 'marvin'")))
	     (format t "Apparently a schema named 'marvin' already exists in the database.~%")
	     (if (check-boolean :db-drop-existing-schema
				"May this schema be dropped so a new one can be created?")
		 (progn
		   (format t "Dropping schema 'marvin'.~%")
		   (dbi:do-sql db-connection "drop database marvin"))
		 (return-from self :abort)))

	   ;; Create schema 'marvin'
	   (setf (piggyback-parameters:get-value :db-schema :file-only) "marvin")
	   (let* ((create-schema-stmt (format nil "create schema ~a" (piggyback-parameters:get-value :db-schema)))
		  (res (dbi:fetch-all
			(dbi:execute
			 (dbi:prepare db-connection create-schema-stmt)))))
	     ;; @TODO Check that it has worked
	     (declare (ignore res)))

	   ;; Grant privileges to user 'marvin' on schema 'marvin'
	   (let* ((grant-stmt (format nil "grant all on ~a.* to '~a'@'~a'"
				      (piggyback-parameters:get-value :db-schema)
				      (piggyback-parameters:get-value :db-username)
				      (if (or (string-equal (piggyback-parameters:get-value :db-host)
							    "localhost")
					      (string-equal (piggyback-parameters:get-value :db-host)
							    "127.0.0.1"))
					  (piggyback-parameters:get-value :db-host)
					  "%")))
	     (res (dbi:fetch-all
		   (dbi:execute
		    (dbi:prepare db-connection grant-stmt)))))
	;; @TODO Check that it has worked
	(declare (ignore res))))
      
      (when db-connection
	(dbi:disconnect db-connection)
	(setf db-connection nil)))

    (unwind-protect
	 (progn
	   (setf db-connection (dbi:connect :mysql
					    :username (piggyback-parameters:get-value :db-admin-username)
					    :password (piggyback-parameters:get-value :db-admin-passwd)
					    :host (piggyback-parameters:get-value :db-host)
					    :database-name (piggyback-parameters:get-value :db-schema)))

	   (unless db-connection
	     (format t "Unable to login to the DB server with user '~a'. Please check your credentials.~%"
                       (piggyback-parameters:get-value :db-admin-username))
	     (return-from self :abort))

	   (let ((result (dbi:execute (dbi:prepare db-connection "SET names 'utf8';"))))
	     ;; Consume the result
	     (loop for row = (dbi:fetch result)
		while row))
	   
	   (labels ((load-statements (filename)
		      (with-open-file (stream filename)
			(read stream))))
	     (iterate:iterate
	       (iterate:for stmt in (load-statements (uiop/common-lisp:merge-pathnames #P"marvin_base_sql.lisp"
										       (asdf:system-source-directory :marvin-installer))))
	       (let ((res (dbi:fetch-all
			  (dbi:execute
			   (dbi:prepare db-connection stmt)))))
		 ;; @TODO Check that it has worked
		 (declare (ignore res))))))

      (when db-connection
	(dbi:disconnect db-connection))))

  ;; Initialize trivial-pooled-database so piggyback-parameters can be used for database storage
  (let ((username (piggyback-parameters:get-value :db-username))
	(passwd (piggyback-parameters:get-value :db-passwd))
	(schema (piggyback-parameters:get-value :db-schema))
	(host (piggyback-parameters:get-value :db-host)))
    (log:info "Initializing DB Connections Pool (~a ~a ~a ~a)"  username passwd schema host)
    (trivial-pooled-database:initialize-connection-pool username passwd schema host))

  ;; Clear DB administrator credentials
  (piggyback-parameters:clear-parameter :db-admin-username)
  (piggyback-parameters:clear-parameter :db-admin-passwd)

  (return-from self :start-marvin))

(define-setup-step :install-plugins
    "Ask if any other plugin should be installed
     Yes - Ask for plugin name and goto :install-user-plugins
     No - Goto :start-marvin"
  (let ((all-projects (remove-if #'null (trivial-gitlab-api:discover-asdf-projects *marvin-plugins-group*))))
    (labels ((add-local-project-version (all-projects)
	       (iterate:iterate
		 (iterate:for projects on all-projects)
		 (trivial-utilities:awhen (asdf:find-system (getf (car projects) :project-defsys) nil)
		   (setf (getf (car projects) :local-version) (asdf:component-version it)))))
	     (list-all-projects (r a)
	       (declare (ignore r a))
	       (iterate:iterate
		 (iterate:for project in all-projects)
		 (format t "ID: ~7@<~d~> Name: ~24A Head Version: ~7A Installed Version: ~7A~%Description: ~{~<~%~13t~1,90:; ~A~>~}~%~%"
			 (getf project :project-id)
			 (getf project :project-name)
			 (getf project :project-version)
			 (trivial-utilities:aif (getf project :local-version)
						it
						"n.a.")
			 (split-sequence:split-sequence #\SPACE (getf project :project-description)))))
	     (show-project-details (all-projects args)
	       (declare (ignore all-projects args))
	       (format t "No details avaliable at this time."))
	     (select-project (r args)
	       (declare (ignore r))
	       (let ((project-id (car args)))
		 (unless project-id
		   (format t "To select a project for installation or update the Project ID is required (e.g. 'select 1234')~%")
		   (return-from select-project))

		 (let ((project (car (member (parse-integer project-id) all-projects :key #'(lambda (x) (getf x :project-id))))))
		   (unless project
		     (format t "No project with the id ~a could be found.~%" project-id)
		     (return-from select-project))

		   (trivial-utilities:aif (member :action project)
					  (setf (cadr it) :select)
					  (nconc project '(:action :select)))
		   (format t "Project with the id ~a is now selected to install/update.~%" project-id))))
	     (deselect-project (r args)
	       (declare (ignore r))
	       (let ((project-id (car args)))
		 (unless project-id
		   (format t "To deselect a project for installation or update the Project ID is required (e.g. 'deselect 1234')~%")
		   (return-from deselect-project))

		 (let ((project (car (member (parse-integer project-id) all-projects :key #'(lambda (x) (getf x :project-id))))))
		   (unless project
		     (format t "No project with the id ~a could be found.~%" project-id)
		     (return-from deselect-project))

		   (trivial-utilities:awhen (member :action project)
		     (setf (cadr it) nil))
		   (format t "Project with the id ~a is now deselected to install/update.~%" project-id))))
	     (view-actions (r a)
	       (declare (ignore r a))
	       (format t "~%")
	       (iterate:iterate
		 (iterate:for project in all-projects)
		 (when (not (null (getf project :action)))
		   (format t "~3t~a (~a) - Action: ~a~%"
			   (getf project :project-name)
			   (getf project :project-id)
			   (if (getf project :local-version)
			       (if (string> (getf project :project-version)
					    (getf project :local-version))
				   "update"
				   "keep local")
			       "install")))))
	     (install-plugins ()
		 (let ((install-plugin-fn (uiop:find-symbol* "INSTALL-PLUGIN" :plugin-handler nil))
		       (initialize-plugin-fn (uiop:find-symbol* "INITIALIZE-PLUGIN" :plugin-base nil))
		       (start-plugin-fn (uiop:find-symbol* "START-PLUGIN" :plugin-base nil))
		       (register-plugin-fn (uiop:find-symbol* "REGISTER-PLUGIN" :plugin-handler nil)))
		   (unless (and install-plugin-fn
				initialize-plugin-fn
				start-plugin-fn)
		     (log:error "Unable to find the necessary functions from Marvin.")
		     (return-from install-plugins))

		   (map 'nil #'(lambda (plugin) (when plugin (funcall start-plugin-fn plugin)))
			(mapcar #'(lambda (plugin-defsys) (when plugin-defsys
							    (trivial-utilities:aprog1
								(funcall initialize-plugin-fn plugin-defsys)
							      (funcall register-plugin-fn it))))
				(mapcar #'(lambda (project) (progn
							      (if (funcall install-plugin-fn project)
								  (getf project :project-defsys)
								  (progn
								    (log:error "Could not install plugin '~a'." (getf project :project-defsys))
								    nil))))
					(remove-if #'(lambda (project) (null (getf project :action)))
						   all-projects))))))
	     (apply-actions (r a)
	       (format t "The following actions are going to be performed:~%")
	       (view-actions r a)
	       (when (check-boolean nil "Do you which to apply these changes?")
		 (install-plugins))))

	(add-local-project-version all-projects)

	(trivial-utilities:aif
	 (member :install-plugins *autodeploy-config*)
	 (progn
	   (dolist (project (cadr it))
	     (select-project nil (list project)))
	   (install-plugins))
	 (progn
	   ;; @TODO Add a text so the user knows what to do next.
	   (format t "~%Please select the plugins to install. Type 'l' to list all avaliable plugins.~%")

	   ;; Create a small repl to show the list of plugins, see plugin details, select plugin for installation, show selected plugins and also deselect selected plugins
	   (let ((repl-light
	     (trivial-repl:create-repl
	      :name "Plugin Selector"
	      :entries `(("List" (#\L #\l) "Show a list of all plugins" ,#'list-all-projects)
			 ("Details" (#\D #\d) "Show details of the plugin with id $id" ,#'show-project-details)
			 ("Select" (#\S #\s) "Select plugin with id $id for installation/update" ,#'select-project)
			 ("Remove" (#\R #\r) "Remove or deselect plugin with id $id" ,#'deselect-project)
			 ("View" (#\V #\v) "Show a list of the selected actions" ,#'view-actions)
			 ("Execute" (#\X #\x) "Execute the selected actions" ,#'apply-actions)))))
	     (trivial-repl:run-repl repl-light))))))

  (return-from self :exit))

(define-setup-step :start-marvin
    "Ask user if marvin should be started"
  (setf (piggyback-parameters:get-value :installed :file-only) t)
  
  (if (check-boolean :start-marvin
		     "Should I start Marvin now?")
      (progn
	(format t "Loading. Please wait...~%")
        (ql:quickload :marvin-base :silent t)
        (let ((fn (uiop:find-symbol* "INITIALIZE" :marvin-base nil)))
          (if fn
	      (progn
	        (funcall fn)
	        (return-from self :install-plugins))

	      (progn
	        (log:error "Marvin initialization function missing. Unable to start.")
	        (return-from self :abort)))))
      ;; @TODO Tell the user plugins are still missing.
      (return-from self :exit)))

(define-setup-step :abort
    ""
  (setf (piggyback-parameters:get-value :installed :file-only) nil)
  (format t "The setup procedure of Marvin AIoT was aborted. None of the changes done so far were reverted.~%Please restart the procedure after applying corrective steps.~%")
  (setf *exit-code* 1)
  (return-from self :exit))

(defun setup-marvin (&key (automated nil))
  ""
  (setf *exit-code* 0)
  (load-autodeploy-config (uiop:merge-pathnames*
			   #P"auto-deployment.cnf"
			   (asdf:system-source-directory :marvin-installer)))

  (when automated
    (when (consp automated)
      (merge-autodeploy-config automated))
    (setf (getf *autodeploy-config* :auto-deploy) t))

  (unless (getf *autodeploy-config* :auto-deploy)
    (setf *autodeploy-config* nil))

  (format t "~%")

  (let ((state :start))
    (handler-bind ((error #'(lambda (c)
				  (log:error "Unhandled condition: ~a" c)
				  (setf *exit-code* 1))))
      (loop
	 until (or (null state) (eq :exit state))
	 do (setf state
		  (trivial-utilities:aif (cdr (assoc state *setup-steps*))
					 (funcall it)
					 :exit)))))
  (return-from setup-marvin *exit-code*))

